<!DOCTYPE html>
<html>

<head>
  
  <title>Ensina Fácil</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karma">
  <link rel="stylesheet" type="text/css" href="css\styleMenu.css">
</head>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Karma", sans-serif}
.w3-bar-block .w3-bar-item {padding:20px}
</style>
<body>
  <?php
    GLOBAL $titulo;
    $titulo = 'Ensina Fácil';
    include 'Geral/cabecalho.php';
  ?>
  
  
<!-- !PAGE CONTENT! -->
<div class="w3-main w3-content w3-padding" style="max-width:900px;margin-top:75px">

  <!-- First Photo Grid-->
  <div class="w3-row-padding w3-padding-16 w3-center" id="food">
    <div class="w3-quarter">
      <a href="Atividades/Alimentos/Alimentos.php"><img src="w3images/sandwich.jpg" alt="Sandwich" style="width:100%">
      <h3>Alimentos</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="#"><img src="w3images/pets.jpg" alt="Steak" style="width:100%">
      <h3>Animais de Estimação</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="#"><img src="w3images/Calendario.jpg" alt="Cherries" style="width:100%">
      <h3>Dias da Semana</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="#"><img src="w3images/Clima.jpg" alt="Pasta and Wine" style="width:100%">
      <h3>Clima</h3></a>
    </div>
  </div>
  
  <!-- Second Photo Grid-->
  <div class="w3-row-padding w3-padding-16 w3-center">
    <div class="w3-quarter">
      <a href="#"><img src="w3images/Saudacoes.jpg" alt="Popsicle" style="width:100%">
      <h3>Saudações</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="#"><img src="w3images/Numeros.jpg" alt="Salmon" style="width:100%">
      <h3>Números</h3>
    </div>

  </div>

  <!-- Pagination -->
  <div class="w3-center w3-padding-32">
    <div class="w3-bar">
      <a href="#" class="w3-bar-item w3-button w3-hover-black">&laquo;</a>
      <a href="#" class="w3-bar-item w3-black w3-button">1</a>
      <a href="#" class="w3-bar-item w3-button w3-hover-black">&raquo;</a>
    </div>
  </div>

<!-- End page content -->
</div>


</body>
</html>
