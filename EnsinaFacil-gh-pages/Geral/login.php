<!doctype html>

<head>
    <title>Ensina Facil</title>
    <meta charset="UTF-8">
    <meta name="viewport"  content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karma">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="..\css\bootstrap.min.css">
    <link rel="stylesheet" href="..\css\text-respossive.css">
    <link rel="stylesheet" href="..\css\font.css">
    <link rel="stylesheet" href="..\css\login.css">
</head>

<body>
    <div><label>&nbsp;</label></div>

    <nav class="navbar navbar-light" width="80%">
        <a class="navbar-brand" href="index.html">
        </a>
    </nav>

    <div><label>&nbsp;</label></div>
    <div><label>&nbsp;</label></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Entre na sua Conta</h5>
                        <form class="form-signin">
                            <div class="form-label-group">
                                <input type="email" id="inputEmail" class="form-control" placeholder="Email ou usuário"
                                    required autofocus>
                                <label for="inputEmail">Email ou usuário</label>
                            </div>

                            <div class="form-label-group">
                                <input type="password" id="inputPassword" class="form-control"
                                    placeholder="Informe sua senha" required>
                                <label for="inputPassword">Senha</label>
                            </div>

                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit"
                                value="Acessar">Entrar</button>

                            <hr class="my-4">
                            <div id="formFooter">
                                <a class="underlineHover" href="cadastro.html">Não tem uma conta?</a> </br>
                                <a class="underlineHover" href="#">Esqueceu a senha?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div><label>&nbsp;</label></div>
    <div><label>&nbsp;</label></div>

    <nav class="navbar navbar-light bg-light, fixed-bottom">

        <label></label>

        <figure class="">
            <figcaption class="figure-caption text-center">Todos os direitos reservados ® Ensina Facil
            </figcaption>

        </figure>

        <label></label>
    </nav>


</body>

</html>